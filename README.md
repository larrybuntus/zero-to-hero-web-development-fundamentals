##### The Items below will have a timeline of 1 month (The whole of May)

---------------------------------------------------------------------------------------------
### Html
##### Resources:
https://www.youtube.com/playlist?list=PL4cUxeGkcC9ivBf_eKCPIAYXWzLlPAm6G

##### Results: 
- Should be able to understand what each tag does
- Should be able to write complex semantic html


### CSS
##### Resources:
- https://sass-lang.com/ - Sass (preferrable)
- https://www.youtube.com/playlist?list=PL4cUxeGkcC9jxJX7vojNVK-o8ubDZEcNb - Sass by net ninja
- https://lesscss.org/ - Less (like sass)
- https://stylus-lang.com/ - Stylus (like sass, just without curly braces and semi colons)

##### Results:
- Should be able to install sass and use sass cli
- Should be able to write nested styles with sass
- Should be able to understand the concept of mixins and functions and it application

###### NB: Timeline for the above is 1 week

---------------------------------------------------------------------------------------------
### Javascript
##### Resources:
- https://www.youtube.com/playlist?list=PL4cUxeGkcC9haFPT7J25Q9GRB_ZkFrQAc - Crash course on javascript by net ninja
- https://www.youtube.com/watch?v=S944-epyYuI - form submission with javascript

##### Results:
- Should be able to get elements from the DOM
- Should be able to add event listeners on elements
- Should be able to submit a form and get the form data
- Should be able to manipulate the DOM and add new items 


At the end of this, we should be able to do a simple todo list. 
Why a TODO list? This will help us
- Understand how to submit a form and get it data
- How to manipulate the DOM and add new items to the DOM
- What elements to use on what part of the API
- How to validate user input 

###### Timeline for the above is 1 week

---------------------------------------------------------------------------------------------


When sucessful with the above. We'll move to more advance javascript libraries like REACT
With this, we'll learn how to do a TODO list the SPA (single page application) way
At the end of this we should be able to

- Understand how components work
- How to reuse components and how to structure your application
- How routing works
- How states work
- How hooks work and when to use them


###### Timeline for the above is 1 week

--------------------------------------------------------------------------------------

After this we'll embark on a larger project that can be put on a portfolio. 
We can do a youtube clone with react and maybe firebase

Timeline for the above is 1 week
